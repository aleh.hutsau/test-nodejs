const path = require('path');
const fs = require('fs');
var glob = require('glob');

const directoryPath = path.join(__dirname);
//passsing directoryPath and callback function
var getDirectories = function (src, callback) {
    glob(src + '/**/*', callback);
  };
  getDirectories(directoryPath, function (err, res) {
    if (err) {
      console.log('Error', err);
    } else {
      console.log(res);
    }
  });